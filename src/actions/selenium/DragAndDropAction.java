package actions.selenium;

import java.util.*;
import actions.selenium.utils.Elements;
import actions.selenium.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

class DragAndDropAction{
    public void run(HashMap<String, Object> params){
    WebElement source = Elements.find(params,Browser.Driver); 
    WebElement destination = Elements.find(params,Browser.Driver); 
    Actions action = new Actions(Browser.Driver);
    action.dragAndDrop(source, destination).build().perform();    
    
    }
}