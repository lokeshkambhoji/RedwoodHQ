package actions.selenium;
import org.openqa.selenium.JavascriptExecutor
import actions.selenium.utils.Elements
import actions.selenium.Browser
import org.openqa.selenium.WebElement

class TypeJavaScript{
    public void run(def params){
          WebElement element = Elements.find(params,Browser.Driver)
        ((JavascriptExecutor)  Browser.Driver).executeScript("arguments[0].setAttribute('value', '"+params.get("Text")+"');",element);
        
    }
}