package actions.selenium;

import java.util.*;
import actions.selenium.utils.Elements;
import actions.selenium.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

class MouseHoverActions{
    public void run(HashMap<String, Object> params){
       WebElement element = Elements.find(params,Browser.Driver);
       Actions builder = new Actions(Browser.Driver);
       builder.moveToElement(element).build().perform();
        
    }
}