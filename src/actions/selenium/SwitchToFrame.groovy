package actions.selenium

import actions.selenium.Browser
import org.openqa.selenium.WebDriver
import actions.selenium.utils.Elements

class SwitchToFrame{
  
  public void run(def params){
  //  WebDriver driver = Browser.Driver
  //  int size = driver.findElements(By.tagName("iframe")).size()
  //  System.out.println(size)  
    
  //  for(int i=0; i<=size; i++){
//	driver.switchTo().frame(i);
//	int total=driver.findElements(By.xpath("//body[@contenteditable='true']//img[@src='https://files.envoke.com/web_files/438/layouts/images/620.jpg']")).size();
//	System.out.println(total)
        
     if(params.Name){
      Browser.Driver.switchTo().frame(params.Name)
    }
    else if(params.Index){
      Browser.Driver.switchTo().frame(params.Index.toInteger())
    }
    
  }
  }
  