package actions.selenium;

import actions.selenium.utils.Elements;
import actions.selenium.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.util.*;

class SafeActionsClick{
    public void run(HashMap<String, Object> params){
	   WebElement element = Elements.find(params,Browser.Driver);
       Actions builder = new Actions(Browser.Driver);
	   builder.moveToElement(element).click().build().perform();
     
    }
}