package actions.selenium.utils

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import actions.Gmail_Locators.Locators;
import org.openqa.selenium.StaleElementReferenceException
import org.testng.Assert
//import org.openqa.selenium.Exception; 

class Elements{
  public static String ID_Name;
  public static WebElement find(def params,WebDriver Driver){
    assert Driver != null, "Error browser is not opened.  Use Open Browser action."
      try{
          ID_Name=Locators.(params.get("ID"));}
          catch(MissingPropertyException e1){
              ID_Name=params.get("ID");
         //Assert.fail(e1.getMessage()+" propert Durgam - StaleElementReferenceException");
      }
      catch(Exception e2){
         Assert.fail(e2.getMessage()+" Mahesh - StaleElementReferenceException");
      }
    WebElement foundElement = null
    switch (params."ID Type"){
      case "Class Name":
      	foundElement = Driver.findElement(By.className(ID_Name))
      	break
      case "Css Selector":
      	foundElement = Driver.findElement(By.cssSelector(ID_Name))
      	break
      case "ID":
      	foundElement = Driver.findElement(By.id(ID_Name))
      	break      
      case "Link Text":
      	foundElement = Driver.findElement(By.linkText(ID_Name))
      	break      
      case "XPath":
      	foundElement = Driver.findElement(By.xpath(ID_Name))
      	break      
      case "Name":
      	foundElement = Driver.findElement(By.name(ID_Name))
      	break      
      case "Partial Link Text":
      	foundElement = Driver.findElement(By.partialLinkText(ID_Name))
      	break      
      case "Tag Name":
      	foundElement = Driver.findElement(By.tagName(ID_Name))
      	break
      default:
        foundElement = Driver.findElement(By.id(ID_Name))
    }
    
    return foundElement
  }
  public static By getByLocator(def params,WebDriver Driver){
    assert Driver != null, "Error browser is not opened.  Use Open Browser action."
	String ID_Name=Locators.(params.get("ID"));
    By foundElement = null
    switch (params."ID Type"){
      case "Class Name":
      	foundElement = By.className(ID_Name)
      	break
      case "Css Selector":
      	foundElement = By.cssSelector(ID_Name)
      	break
      case "ID":
      	foundElement = By.id(ID_Name)
      	break      
      case "Link Text":
      	foundElement = By.linkText(ID_Name)
      	break      
      case "XPath":
      	foundElement = By.xpath(ID_Name)
      	break      
      case "Name":
      	foundElement = By.name(ID_Name)
      	break      
      case "Partial Link Text":
      	foundElement = By.partialLinkText(ID_Name)
      	break      
      case "Tag Name":
      	foundElement =By.tagName(ID_Name)
      	break
      default:
        foundElement = By.id(ID_Name)
    }
    
    return foundElement
  }
  public static def findAll(def params,WebDriver Driver){
  //  assert Driver != null, "Error browser is not opened.  Use Open Browser action."
 //   String ID_Name=Locators.(params.get("ID"));
    def foundElements = []
    
    switch (params."ID Type"){
      case "Class Name":
      	foundElements = Driver.findElements(By.className(ID_Name))
      	break
      case "Css Selector":
      	foundElements = Driver.findElements(By.cssSelector(ID_Name))
      	break
      case "ID":
      	foundElements = Driver.findElements(By.id(ID_Name))
      	break      
      case "Link Text":
      	foundElements = Driver.findElements(By.linkText(ID_Name))
      	break      
      case "XPath":
      	foundElements = Driver.findElements(By.xpath(ID_Name))
      	break      
      case "Name":
      	foundElements = Driver.findElements(By.name(ID_Name))
      	break      
      case "Partial Link Text":
      	foundElements = Driver.findElements(By.partialLinkText(ID_Name))
      	break      
      case "Tag Name":
      	foundElements = Driver.findElements(By.tagName(ID_Name))
      	break
      default:
        foundElements = Driver.findElements(By.id(ID_Name))
    }
    
    return foundElements
  }
}