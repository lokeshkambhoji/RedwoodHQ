package actions.selenium;

import java.util.*;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.AWTException;

class HitEnter{
    public void run(HashMap<String, Object> params){
    	try
		{
        Robot _robot = new Robot();		
        _robot.keyPress(KeyEvent.VK_ENTER);
        System.out.println("Clicked on the ENTER button");
        }
        catch(Exception e)
		{
			e.printStackTrace();
		}
    }
}