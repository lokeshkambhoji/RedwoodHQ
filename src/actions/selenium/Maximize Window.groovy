package actions.selenium;

import actions.selenium.utils.Elements
import actions.selenium.Browser
import org.openqa.selenium.WebDriver

class MaximizeWindow{
    public WebDriver driver = Browser.Driver;
    public void run(def params){
     driver.manage().window().maximize() 
        
    }
}