package actions.selenium;

import java.util.*;
import actions.selenium.utils.Elements;
import actions.selenium.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

class SafeDoubleClickAction{
    public void run(HashMap<String, Object> params){
       WebElement element = Elements.find(params,Browser.Driver);
       Actions userAction = new Actions(Browser.Driver).doubleClick(element);
	   userAction.build().perform();
    }
}