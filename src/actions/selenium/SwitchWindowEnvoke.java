package actions.selenium;

import java.util.*;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.AWTException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

class SwitchWindowEnvoke{
 public WebDriver driver = Browser.Driver;    
    public void run(HashMap<String, Object> params) throws Exception{
     driver.findElement(By.xpath("//table[@align='right']//a[@title='Browse Server']")).click();
     String Parent_Window = driver.getWindowHandle();
  
    for (String Child_Window : driver.getWindowHandles())
    {
 	 driver.switchTo().window(Child_Window);
     System.out.println("Switched to child window from parent window");
     driver.switchTo().frame(0);
     System.out.println("Switched to the iframe");
     Thread.sleep(3000);   
 	 Actions action = new Actions(driver);
     //driver.findElement(By.xpath("//a[@id='r2' and @title='bali4_160x107.png']")).click();
     action.moveToElement(driver.findElement(By.xpath("//a[@id='r2' and @title='bali4_160x107.png']"))).doubleClick().build().perform();
     System.out.println("Double clicked on the Bali Image");
//     try
//		{
//        Robot _robot = new Robot();		
//        _robot.keyPress(KeyEvent.VK_ENTER);
//        Thread.sleep(3000);    
//        System.out.println("Clicked on the ENTER button");
//        }
//        catch(Exception e)
//		{
//			e.printStackTrace();
//		}
       driver.switchTo().window(Parent_Window);
 	   System.out.println("Switched back to parent window");
       driver.findElement(By.xpath("//table[@role='presentation']//a[@title='OK']")).click(); 
       
    }
 }       
}